# C2Metadata all-in-one Brown Dog converter

Based on https://opensource.ncsa.illinois.edu/bitbucket/projects/BD/repos/converters-template/

* `Dockerfile`: builds `c2metadata-bd` image
* `docker-compose.yml`: starts BD/polyglot componets
* `c2metadata_convert.py`: wrapper script 
* examples: Example input zip files for testing

Note: This uses a custom polyglot image due to a mongo configuration problem.

This repo relies on submodules:

```
git submodule update --init --recursive .
```


```
docker build -t c2metadata-bd -f Dockerfile ..
```

```
docker-compose up
```

Currently working tests:
```
curl -F "File=@examples/small-stata.zip" http://localhost:8184/convert/c2metadata.zip
curl -F "File=@examples/small-python.zip" http://localhost:8184/convert/c2metadata.zip
curl -F "File=@examples/gss-36797-R.zip" http://localhost:8184/convert/c2metadata.zip
curl -F "File=@examples/gss-36797-spss.zip" http://localhost:8184/convert/c2metadata.zip
curl -F "File=@examples/gss-36797-stata.zip" http://localhost:8184/convert/c2metadata.zip
```

## Input file format

The input file for C2Metadata conversion must contain the following:
* Script file in a supported language (STATA, SAS, R, Python, SPSS)
* Original metadata file in a supported format (e.g., DDI, EML)
* `input.json` file describing the script, metadata file, and dataset name.

For example (`small_test.zip`):
```
{
    "script": "small_test.do",
    "datasets": [
        {
            "filename": "36437-0001subset",
            "metadata": "36437-0001subset.xml"
        }
    ]
}
``` 

